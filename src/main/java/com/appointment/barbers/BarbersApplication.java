package com.appointment.barbers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.appointment.barbers")
public class BarbersApplication {

	public static void main(String[] args) {
		SpringApplication.run(BarbersApplication.class, args);
	}

}
