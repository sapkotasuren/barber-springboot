package com.appointment.barbers.controllers;

import com.appointment.barbers.model.Appointment;
import com.appointment.barbers.service.AppointmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/barber")
public class AppointmentController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private AppointmentService appointmentService;

    public AppointmentController(AppointmentService appointmentService) {
        this.appointmentService = appointmentService;
    }

    @PostMapping(value = "/{barberId}/appointment")
    @ResponseStatus(HttpStatus.CREATED)
    public Appointment saveAppointment(@PathVariable Long barberId, @RequestBody Appointment appointment){
        return this.appointmentService.save(appointment);
    }

    @GetMapping(value = "/{barberId}/appointment/{appointmentId}")
    @ResponseStatus(HttpStatus.OK)
    public Appointment getAppointmentById(@PathVariable Long barberId, @PathVariable Long appointmentId) {
        return this.appointmentService.findAppointmentByBarberIdAndAppointmentId(barberId,appointmentId);
    }

    @GetMapping(value = "/{barberId}/appointment")
    @ResponseStatus(HttpStatus.OK)
    public List<Appointment> getAllAppointment(@PathVariable Long barberId){
        return this.appointmentService.findAllByBarberIdOrderByDateDesc(barberId);
    }

    @PutMapping(value = "/{barberId}/appointment/{appointmentId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Appointment updateAppointment(@PathVariable Long barberId, Long appointmentId, @RequestBody Appointment appointment) {
        return this.appointmentService.update(appointment);
    }

    @DeleteMapping(value = "/{barberId}/appointment/{appointmentId}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteAppointment(@PathVariable Long barberId, @PathVariable Long appointmentId) {
        this.appointmentService.deleteByAppointmentId(appointmentId);
    }




}
