package com.appointment.barbers.controllers;

import com.appointment.barbers.model.Appointment;
import com.appointment.barbers.service.AppointmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/api/barber/appointment")
public class BarberAppointmentController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private AppointmentService appointmentService;

    public BarberAppointmentController(AppointmentService appointmentService) {
        this.appointmentService = appointmentService;
    }

    @PostMapping
    public Appointment createAppointment(@RequestBody Appointment appointment) {
        logger.debug("Creating the appointment: "+appointment);
        Appointment savedAppointment = appointmentService.save(appointment);
        logger.info("Appointment created: "+savedAppointment.toString());
        return savedAppointment;
    }
}
