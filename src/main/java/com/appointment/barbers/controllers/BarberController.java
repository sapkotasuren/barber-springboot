package com.appointment.barbers.controllers;

import com.appointment.barbers.model.Barber;
import com.appointment.barbers.service.BarberService;
import com.appointment.barbers.service.exception.ForbiddenException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/barber")
public class BarberController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private BarberService barberService;

    public BarberController(BarberService barberService) {
        this.barberService = barberService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Barber saveBarber(@RequestBody Barber barber) {
        logger.debug("Starting to save the object: "+barber.toString());
        Barber savedBarber = this.barberService.save(barber);
        logger.info("Barber is saved: "+savedBarber.toString());
        return savedBarber;
    }

    @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    public List<Barber> getBarberList() {
        List<Barber> barberList = this.barberService.findAllBarbers();
        return barberList;
    }

    @GetMapping("/{barberId}")
    @ResponseStatus(HttpStatus.OK)
    public Barber getBarber(@PathVariable Long barberId){
        logger.debug("getBarber barberId: "+ barberId);
        Barber barber = this.barberService.findBarberById(barberId);
        return  barber;
    }

    @PutMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Barber updateBarber(@RequestBody Barber barber) {
        Barber updatedBarber = this.barberService.updateBarber(barber);
        return updatedBarber;
    }

    @DeleteMapping(value = "/{barberId}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteBarber(@PathVariable Long barberId) {
        this.barberService.deleteBarber(barberId);
    }
}