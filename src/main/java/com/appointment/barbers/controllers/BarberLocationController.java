package com.appointment.barbers.controllers;

import com.appointment.barbers.model.Barber;
import com.appointment.barbers.model.BarberLocation;
import com.appointment.barbers.service.BarberLocationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/api/barber")
public class BarberLocationController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private BarberLocationService barberLocationService;

    public BarberLocationController(BarberLocationService barberLocationService) {
        this.barberLocationService = barberLocationService;
    }

    @PostMapping(value = "/{barberId}/location")
    @ResponseStatus(HttpStatus.CREATED)
    public BarberLocation saveBarberLocation(@PathVariable Long barberId, @RequestBody BarberLocation barberLocation) {
        logger.info("Pathvariable of barberId: "+barberId);
        logger.info("Barber location to save: "+ barberLocation.toString());
        BarberLocation savedBarberLocation = this.barberLocationService.save(barberLocation);
        return savedBarberLocation;
    }

    @GetMapping(value = "/{barberId}/location/{locationId}")
    @ResponseStatus(HttpStatus.OK)
    public BarberLocation getBarberLocation(@PathVariable Long barberId, @PathVariable Long locationId){
        logger.info("barberLocation id: "+locationId);
        BarberLocation barberLocation = this.barberLocationService.findBarberLocationById(locationId);
        return barberLocation;
    }
    @GetMapping(value = "/{barberId}/location")
    @ResponseStatus(HttpStatus.OK)
    public BarberLocation getBarberLocationByBarberId(@PathVariable Long barberId){
        logger.info("barberId to find the location "+ barberId);
        BarberLocation barberLocation = this.barberLocationService.findByBarberId(barberId);
        return  barberLocation;
    }
    @PutMapping(value = "/{barberId}/location")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public BarberLocation updateBarberLocation(@PathVariable Long barberId, @RequestBody BarberLocation barberLocation) {
        logger.info("This is the barber location we are trying to update: "+barberLocation.toString());
        BarberLocation updatedBarberLocation = this.barberLocationService.updateBarberLocation(barberLocation);
        return updatedBarberLocation;
    }

    @DeleteMapping(value = "/{barberId}/location/{barberLocationId}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteBarberlocation(@PathVariable Long barberId, @PathVariable Long barberLocationId){
        this.barberLocationService.deleteBarberLocation(barberLocationId);
    }

}