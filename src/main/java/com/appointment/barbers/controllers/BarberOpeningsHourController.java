package com.appointment.barbers.controllers;

import com.appointment.barbers.model.OpeningsHour;
import com.appointment.barbers.service.BarberOpeningsHourService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/barber")
public class BarberOpeningsHourController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private BarberOpeningsHourService barberOpeningsHourService;

    public BarberOpeningsHourController(BarberOpeningsHourService barberOpeningsHourService) {
        this.barberOpeningsHourService = barberOpeningsHourService;
    }

    @PostMapping(value = "/{barberId}/openingsHour")
    @ResponseStatus(HttpStatus.CREATED)
    public OpeningsHour saveOpeningsHour(@PathVariable Long barberId, @RequestBody OpeningsHour openingsHour){
        logger.info("Pathvariable of barberId: "+barberId);
        OpeningsHour savedOpeningsHour = this.barberOpeningsHourService.save(openingsHour);
        return savedOpeningsHour;
    }
    @PutMapping(value = "/{barberId}/openingsHour")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public OpeningsHour updateOpeningsHour(@PathVariable Long barberId, @RequestBody OpeningsHour openingsHour) {
        logger.info("updating openingshours openingshourId: "+openingsHour.getOpeningsHourId());
        OpeningsHour updatedOpeningshour = this.barberOpeningsHourService.update(openingsHour);
        return updatedOpeningshour;
    }

    @GetMapping(value= "/{barberId}/openingsHour/{openingsHourId}")
    @ResponseStatus(HttpStatus.OK)
    public OpeningsHour getOpeningsHourById(@PathVariable Long barberId, @PathVariable Long openingsHourId) {
        logger.info("Fetching the openingshour of id: "+openingsHourId);
        return this.barberOpeningsHourService.findByOpeningsHourId(openingsHourId);
    }

    @GetMapping(value = "/{barberId}/openingsHour")
    @ResponseStatus(HttpStatus.OK)
    public List<OpeningsHour> getOpeningHoursByBarberId(@PathVariable Long barberId){
        List<OpeningsHour> openingsHourList = this.barberOpeningsHourService.findByBarberId(barberId);
        return openingsHourList;
    }

    @DeleteMapping(value = "/{barberId}/openingsHour/{openingsHourId}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteOpeningshour(@PathVariable Long barberId, @PathVariable Long openingsHourId) {
        this.barberOpeningsHourService.deleteOpeningshourById(openingsHourId);
    }
}
