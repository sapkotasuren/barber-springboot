package com.appointment.barbers.controllers;

import com.appointment.barbers.model.BarberService;
import com.appointment.barbers.service.BarberServiceService;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/barber")
public class BarberServiceController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private BarberServiceService barberServiceService;

    public BarberServiceController(BarberServiceService barberServiceService) {
        this.barberServiceService = barberServiceService;
    }

    @PostMapping(value = "/{barberId}/service")
    @ResponseStatus(HttpStatus.CREATED)
    public BarberService saveBarberService(@PathVariable Long barberId, @RequestBody BarberService barberService) {
        logger.info("Pathvariable of barberId: "+barberId);
        logger.info("Barber service to save: "+barberService.toString());
        BarberService savedBarberService = this.barberServiceService.save(barberService);
        return savedBarberService;
    }
    @GetMapping(value = "/{barberId}/service/{barberServiceId}")
    @ResponseStatus(HttpStatus.OK)
    public BarberService getBarberService(@PathVariable Long barberId, @PathVariable Long barberServiceId) {
        logger.info("barberService id: "+barberServiceId +" barberId: "+barberId);
        BarberService barberService = this.barberServiceService.findBarberServiceByBarberIdAndBarberServiceId(barberId,barberServiceId);
        return barberService;
    }
    @GetMapping(value = "/{barberId}/service")
    @ResponseStatus(HttpStatus.OK)
    public List<BarberService> getBarberServices(@PathVariable Long barberId) {
        logger.info("BarberId to find the barberservices: "+barberId);
        List<BarberService> barberServices = this.barberServiceService.findBarberServiceByBarberId(barberId);
        return barberServices;
    }
    @PutMapping(value = "/{barberId}/service")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public BarberService updateBarberService(@PathVariable Long barberId, @RequestBody BarberService barberService) {
        logger.info("This is the barber service, we are trying to update: "+barberService.toString());
        BarberService updatedBarberService = this.barberServiceService.updateBarberServiceByBarberServiceId(barberService);
        return barberService;
    }
    @DeleteMapping(value = "/{barberId}/service/{barberServiceId}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteBarberService(@PathVariable Long barberId, @PathVariable Long barberServiceId) {
        this.barberServiceService.deleteBarberServiceByBarberServiceId(barberServiceId);
    }

}
