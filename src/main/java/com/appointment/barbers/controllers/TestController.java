package com.appointment.barbers.controllers;

import com.appointment.barbers.model.Appointment;
import com.appointment.barbers.model.Barber;
import com.appointment.barbers.model.enums.Gender;
import com.appointment.barbers.repository.BarberRepository;
import com.appointment.barbers.service.AppointmentService;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

@RestController
@CrossOrigin
public class TestController {


    private AppointmentService appointmentService;
    private BarberRepository barberRepository;

    public TestController(AppointmentService appointmentService, BarberRepository barberRepository) {
        this.appointmentService = appointmentService;
        this.barberRepository = barberRepository;
    }

    @GetMapping("/test")
    public String testMethod(){
        return  "hello hey hey";
    }

    @GetMapping("/barber")
    public Appointment createAppointment() {
        Appointment app = new Appointment();
        app.setBarberId(1L);
        app.setBarberServiceId(1L);
        app.setEmail("suren@hotmail.com");
        app.setName("suren");
        app.setGender(Gender.M);
        app.setDate(LocalDate.now());
        app.setMobilePhone("6727");
        app = appointmentService.save(app);
        return app;
    }

    @PostMapping("/barber")
    public Barber registerBarber(@RequestBody Barber barber) {
        System.out.println(barber.toString());
       Barber b =  this.barberRepository.save(barber);
        System.out.println(b.toString());
        return b;
    }

}

//insert into barber(name, description, ismen,iswomen)values('test','test',true,true);
//
//insert into barber_service(name, description, price,gender,barber_id_fk)values('service','servicetest',10.00,'M',1);