package com.appointment.barbers.model;

import com.appointment.barbers.model.enums.Gender;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "appointment")
public class Appointment {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "appointment_id")
    private Long appointmentId;
    @Column(name = "name")
    private String name;
    @Column(name = "email")
    private String email;
    @Column(name = "mobile_phone")
    private String mobilePhone;
    @Column(name = "gender")
    @Enumerated(EnumType.STRING)
    private Gender gender;
    @Column(name = "date_time")
    private LocalDate date;
    @Column(name = "barber_id_fk")
    private Long barberId;
    @Column(name = "barber_service_id_fk")
    private Long barberServiceId;

    public Appointment() {}

    public Appointment(String name, String email, String mobilePhone, Gender gender, LocalDate date, Long barberId, Long barberServiceId) {
        this.name = name;
        this.email = email;
        this.mobilePhone = mobilePhone;
        this.gender = gender;
        this.date = date;
        this.barberId = barberId;
        this.barberServiceId = barberServiceId;
    }

    public Long getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(Long appointmentId) {
        this.appointmentId = appointmentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Long getBarberId() {
        return barberId;
    }

    public void setBarberId(Long barberId) {
        this.barberId = barberId;
    }

    public Long getBarberServiceId() {
        return barberServiceId;
    }

    public void setBarberServiceId(Long barberServiceId) {
        this.barberServiceId = barberServiceId;
    }

    @Override
    public String toString() {
        return "Appointment{" +
                "appointmentId=" + appointmentId +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", mobile_phone='" + mobilePhone + '\'' +
                ", gender=" + gender +
                ", dateAndTime=" + date +
                ", barber_id=" + barberId +
                ", barber_service_id=" + barberServiceId +
                '}';
    }
}