package com.appointment.barbers.model;

import java.time.LocalDate;
import java.time.LocalTime;

public class AppointmentAvailableHour {
    //let say for one specific date this time available
    private LocalDate date;
    //let say for that day specific this time availabel.
    private String day;
    private LocalTime time;
    private int numberOfPlaces;

    public AppointmentAvailableHour(){}

    public AppointmentAvailableHour(LocalDate date, String day, LocalTime time, int numberOfPlaces) {
        this.date = date;
        this.day = day;
        this.time = time;
        this.numberOfPlaces = numberOfPlaces;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public int getNumberOfPlaces() {
        return numberOfPlaces;
    }

    public void setNumberOfPlaces(int numberOfPlaces) {
        this.numberOfPlaces = numberOfPlaces;
    }
}
