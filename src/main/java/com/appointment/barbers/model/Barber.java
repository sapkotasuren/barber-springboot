package com.appointment.barbers.model;

import javax.persistence.*;

@Entity
@Table(name = "barber")

public class Barber {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "barber_id")
    private Long barberId;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "ismen")
    private boolean isMen;
    @Column(name = "iswomen")
    private boolean isWomen;

    public Barber() {}

    public Barber(String name, String description, boolean isMen, boolean isWomen) {
        this.name = name;
        this.description = description;
        this.isMen = isMen;
        this.isWomen = isWomen;
    }

    public Long getBarberId() {
        return barberId;
    }

    public void setBarberId(Long barberId) {
        this.barberId = barberId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getIsMen() {
        return isMen;
    }

    public void setIsMen(boolean men) {
        isMen = men;
    }

    public boolean getIsWomen() {
        return isWomen;
    }

    public void setIsWomen(boolean women) {
        isWomen = women;
    }

    @Override
    public String toString() {
        return "Barber{" +
                "barberId=" + barberId +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", isMen=" + isMen +
                ", isWomen=" + isWomen +
                '}';
    }
}