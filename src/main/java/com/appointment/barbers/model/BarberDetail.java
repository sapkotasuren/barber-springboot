package com.appointment.barbers.model;

public class BarberDetail {
    private Long barberDetailId;
    private String naam;
    private String vennootschap;
    private String btwNumber;
    private String email;
    private String telephoneNumber;
    private String mobileNumber;
    private String accountNumber;

    public BarberDetail(){}

    public BarberDetail(Long barberDetailId, String naam, String vennootschap, String btwNumber, String email, String telephoneNumber, String mobileNumber, String accountNumber) {
        this.naam = naam;
        this.vennootschap = vennootschap;
        this.btwNumber = btwNumber;
        this.email = email;
        this.telephoneNumber = telephoneNumber;
        this.mobileNumber = mobileNumber;
        this.accountNumber = accountNumber;
    }

    public Long getBarberDetailId() {
        return barberDetailId;
    }

    public void setBarberDetailId(Long barberDetailId) {
        this.barberDetailId = barberDetailId;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public String getVennootschap() {
        return vennootschap;
    }

    public void setVennootschap(String vennootschap) {
        this.vennootschap = vennootschap;
    }

    public String getBtwNumber() {
        return btwNumber;
    }

    public void setBtwNumber(String btwNumber) {
        this.btwNumber = btwNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
}
