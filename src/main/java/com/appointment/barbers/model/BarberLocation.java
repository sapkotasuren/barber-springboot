package com.appointment.barbers.model;

import javax.persistence.*;

@Entity
@Table(name = "barber_location")
public class BarberLocation {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "barber_location_id")
    private Long barberLocationId;
    @Column(name = "street")
    private String street;
    @Column(name = "house_number")
    private String houseNumber;
    @Column(name = "bus")
    private String bus;
    @Column(name = "postcode")
    private String postCode;
    @Column(name = "city")
    private String city;
    @Column(name = "country")
    private String country;
    @Column(name = "barber_id_fk")
    private Long barberId;

    public BarberLocation(){}

    public BarberLocation(String street, String houser_number, String bus, String postcode, String city, String country, Long barberId) {
        this.street = street;
        this.houseNumber = houser_number;
        this.bus = bus;
        this.postCode = postcode;
        this.city = city;
        this.country = country;
        this.barberId = barberId;
    }

    public Long getBarberLocationId() {
        return barberLocationId;
    }

    public void setBarberLocationId(Long barberLocationId) {
        this.barberLocationId = barberLocationId;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getBus() {
        return bus;
    }

    public void setBus(String bus) {
        this.bus = bus;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Long getBarberId() {
        return barberId;
    }

    public void setBarberId(Long barberId) {
        this.barberId = barberId;
    }

    @Override
    public String toString() {
        return "BarberLocation{" +
                "barberLocationId=" + barberLocationId +
                ", street='" + street + '\'' +
                ", house_number='" + houseNumber + '\'' +
                ", bus='" + bus + '\'' +
                ", postCode='" + postCode + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", barberId=" + barberId +
                '}';
    }
}