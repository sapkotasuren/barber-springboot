package com.appointment.barbers.model;

import com.appointment.barbers.model.enums.Gender;

import javax.persistence.*;

@Entity
@Table(name = "barber_service")
public class BarberService {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "barber_service_id")
    private Long barberServiceId;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "price")
    private double price;
    @Column(name = "gender")
    private Gender gender;
    @Column(name = "barber_id_fk")
    private Long barberId;

    public BarberService(){}

    public BarberService(String name, String description, double price, Gender gender, Long barberId ){
        this.name = name;
        this.description = description;
        this.price = price;
        this.gender = gender;
        this.barberId = barberId;
    }

    public Long getBarberServiceId() {
        return barberServiceId;
    }

    public void setBarberServiceId(Long barberServiceId) {
        this.barberServiceId = barberServiceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Long getBarberId() {
        return barberId;
    }

    public void setBarberId(Long barberId) {
        this.barberId = barberId;
    }

    @Override
    public String toString() {
        return "BarberService{" +
                "barberServiceId=" + barberServiceId +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", gender=" + gender +
                ", barberId=" + barberId +
                '}';
    }
}