package com.appointment.barbers.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "openings_hour")
public class OpeningsHour {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "openings_hour_id")
    private Long openingsHourId;
    @Column(name = "day")
    private String day;
    @Column(name = "m_from")
    private LocalDateTime m_from;
    @Column(name = "m_till")
    private LocalDateTime m_till;
    @Column(name = "a_from")
    private LocalDateTime a_from;
    @Column(name = "a_till")
    private LocalDateTime a_till;
    @Column(name = "isclose")
    private boolean isclose;
    @Column(name = "barber_id_fk")
    private Long barberId;

    public OpeningsHour(){}

    public OpeningsHour(String day, LocalDateTime m_from, LocalDateTime m_till, LocalDateTime a_from, LocalDateTime a_till, boolean isclose, Long barberId) {
        this.day = day;
        this.m_from = m_from;
        this.m_till = m_till;
        this.a_from = a_from;
        this.a_till = a_till;
        this.barberId = barberId;
    }

    public Long getOpeningsHourId() {
        return openingsHourId;
    }

    public void setOpeningsHourId(Long openingsHourId) {
        this.openingsHourId = openingsHourId;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public LocalDateTime getM_from() {
        return m_from;
    }

    public void setM_from(LocalDateTime m_from) {
        this.m_from = m_from;
    }

    public LocalDateTime getM_till() {
        return m_till;
    }

    public void setM_till(LocalDateTime m_till) {
        this.m_till = m_till;
    }

    public LocalDateTime getA_from() {
        return a_from;
    }

    public void setA_from(LocalDateTime a_from) {
        this.a_from = a_from;
    }

    public LocalDateTime getA_till() {
        return a_till;
    }

    public void setA_till(LocalDateTime a_till) {
        this.a_till = a_till;
    }

    public boolean isIsclose() {
        return isclose;
    }

    public void setIsclose(boolean isclose) {
        this.isclose = isclose;
    }

    public Long getBarberId() {
        return barberId;
    }

    public void setBarberId(Long barberId) {
        this.barberId = barberId;
    }

    @Override
    public String toString() {
        return "OpeningsHour{" +
                "openings_hour_id=" + openingsHourId +
                ", day='" + day + '\'' +
                ", m_from=" + m_from +
                ", m_till=" + m_till +
                ", a_from=" + a_from +
                ", a_till=" + a_till +
                ", isClose=" + isclose +
                ", barber_id=" + barberId +
                '}';
    }
}