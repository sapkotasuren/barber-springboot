package com.appointment.barbers.model.enums;

public enum Gender {
    /*M stand Male, F stand for Female*/
    //M,F
     M ("M"),
    F ("F")
    ;
   private String value;
    Gender(String value) {
        this.value = value;
    }

    public String getValue(){
        return value;
    }
}
