package com.appointment.barbers.repository;

import com.appointment.barbers.model.Appointment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AppointmentRepository extends CrudRepository<Appointment, Long> {

    Appointment save(Appointment appointment);
    Appointment findByAppointmentId(Long appointmentId);
    List<Appointment> findAllByBarberIdOrderByDateDesc(Long barberId);
    Appointment findAppointmentByBarberIdAndAppointmentId(Long barberId, Long appointmentId);
    void deleteByAppointmentId(Long appointmentId);
}