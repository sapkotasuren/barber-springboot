package com.appointment.barbers.repository;

import com.appointment.barbers.model.Barber;
import com.appointment.barbers.model.BarberLocation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BarberLocationRepository extends CrudRepository<BarberLocation, Long> {
    BarberLocation save(BarberLocation barberLocation);
    BarberLocation findByBarberLocationId(Long barberLocationId);
    BarberLocation findByBarberId(Long barberId);
    List<BarberLocation> findByPostCode(String postcode);
    void deleteByBarberLocationId(Long barberLocationId);
    //this is not good because we don't access barber through barberLocation. Better inject barberservice in barberRepo if you need it!
    Barber findBarberByBarberId(Long barberId);
}
