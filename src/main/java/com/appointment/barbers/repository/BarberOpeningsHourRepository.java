package com.appointment.barbers.repository;

import com.appointment.barbers.model.Barber;
import com.appointment.barbers.model.OpeningsHour;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BarberOpeningsHourRepository extends CrudRepository<OpeningsHour, Long> {
    OpeningsHour save(OpeningsHour openingsHour);
    List<OpeningsHour> findByBarberId(Long barberId);
    List<OpeningsHour> save (List<OpeningsHour> openingsHours);
    OpeningsHour findByOpeningsHourId(Long openingsHourId);
    void deleteByOpeningsHourId(Long OpeningHourId);
}