package com.appointment.barbers.repository;

import com.appointment.barbers.model.Appointment;
import com.appointment.barbers.model.Barber;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BarberRepository extends CrudRepository<Barber, Long> {

    Barber save(Barber barber);
    Barber findByBarberId(Long barberId);
    List<Barber> findByIsMen(boolean isMen);
    List<Barber> findAll();
}
