package com.appointment.barbers.repository;

import com.appointment.barbers.model.BarberLocation;
import com.appointment.barbers.model.BarberService;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BarberServiceRepository extends CrudRepository<BarberService, Long> {

    BarberService save(BarberService barberService);
    BarberService findBarberServiceByBarberIdAndBarberServiceId(Long barberId, Long barberServiceId);
    BarberService findBarberServiceByBarberServiceId(Long barberServiceId);
    List<BarberService> findBarberServiceByBarberId(Long barberId);
    void deleteBarberServiceByBarberId(Long barberId);
    void deleteBarberServiceByBarberServiceId(Long barberServiceId);
}
