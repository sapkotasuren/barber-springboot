package com.appointment.barbers.service;

import com.appointment.barbers.model.Appointment;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface AppointmentService {
    Appointment save(Appointment appointment);
    Appointment findAppointmentById(Long appointmentId);
    List<Appointment> findAllByBarberIdOrderByDateDesc(Long barberId);
    Appointment findAppointmentByBarberIdAndAppointmentId(Long barberId, Long appointmentId);
    Appointment update(Appointment appointment);
    void deleteByAppointmentId(Long appointmentId);
}