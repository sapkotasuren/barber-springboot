package com.appointment.barbers.service;

import com.appointment.barbers.model.BarberLocation;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface BarberLocationService {
    BarberLocation save (BarberLocation barberLocation);
    BarberLocation findBarberLocationById (Long barberLocationId);
    BarberLocation updateBarberLocation (BarberLocation barberLocation);
    void deleteBarberLocation (Long barberLocationId);
    BarberLocation findByBarberId(Long barberId);
    List<BarberLocation> findByPostcode(String postcode);
}
