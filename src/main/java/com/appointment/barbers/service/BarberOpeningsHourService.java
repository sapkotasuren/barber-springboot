package com.appointment.barbers.service;

import com.appointment.barbers.model.OpeningsHour;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface BarberOpeningsHourService {
    OpeningsHour save (OpeningsHour openingsHour);
    OpeningsHour findByOpeningsHourId(Long openingsHourId);
    List<OpeningsHour> findByBarberId(Long barberId);
    OpeningsHour update (OpeningsHour openingsHour);
    List<OpeningsHour> saveOpeningsHours(List<OpeningsHour>openingsHours);
    List<OpeningsHour> update(List<OpeningsHour> openingsHours);
    void deleteOpeningshourById(Long openingsHourId);
}
