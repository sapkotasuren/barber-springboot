package com.appointment.barbers.service;

import com.appointment.barbers.model.Barber;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface BarberService {
    Barber save (Barber barber);
    List<Barber> findAllMenBarber();
    List<Barber> findAllBarbers();
    Barber findBarberById(Long barberId);
    Barber updateBarber(Barber barber);
    void deleteBarber(Long barberId);
}