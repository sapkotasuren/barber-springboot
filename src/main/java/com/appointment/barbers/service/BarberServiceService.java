package com.appointment.barbers.service;

import com.appointment.barbers.model.BarberService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface BarberServiceService {
    BarberService save(BarberService barberService);
    BarberService findBarberServiceByBarberIdAndBarberServiceId(Long barberId, Long barberServiceId);
    BarberService findBarberServiceByBarberServiceId(Long barberServiceId);
    List<BarberService> findBarberServiceByBarberId(Long barberId);
    void deleteBarberServiceByBarberId(Long barberId);
    void deleteBarberServiceByBarberServiceId(Long barberServiceId);
    BarberService updateBarberServiceByBarberServiceId(BarberService barberService);

}
