package com.appointment.barbers.service.email;

import org.springframework.stereotype.Service;

public interface EmailService {

     void send(String recipientEmail, String subject, String message);
     void sendUsingMimeMessage(String recipientEmail, String subject, String message);
}
