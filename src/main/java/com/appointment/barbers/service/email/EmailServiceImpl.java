package com.appointment.barbers.service.email;

import io.micrometer.core.instrument.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMailMessage;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class EmailServiceImpl implements EmailService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private JavaMailSender emailSender;

    public EmailServiceImpl(JavaMailSender emailSender) {
        this.emailSender = emailSender;
    }

    @Override
    public void send(String recipientEmail, String subject, String message) {
        logger.info("sending email to "+recipientEmail + " Subject: "+subject+" message:"+message);
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setFrom("noreply@barberSystem.com");
        msg.setTo(recipientEmail);
        msg.setSubject(subject);
        msg.setText(message);
        emailSender.send(msg);
    }

    @Override
    public void sendUsingMimeMessage(String recipientEmail, String subject, String message) {
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true);
            messageHelper.setFrom("noreply@barberSystem.com");
            List<String> to = new ArrayList<>();
            to.add("sapkotasuren@hotmail.com");
            to.add("sumon@sapkota.be");
            to.add("aayush@sapkota.be");
            to.add(recipientEmail);
            String[] toMailList = to.toArray(new String[to.size()]);
            messageHelper.setTo(toMailList); //if you put email on this way every one will see it who else has receive this message.
            messageHelper.setSubject(subject);
            message.concat("<h1>hehe</h1>");
            messageHelper.setSentDate(Date.valueOf(LocalDate.of(1999,12,19)));
            messageHelper.setText(message, true);
            String attachmentUrl = "";
            if (StringUtils.isNotEmpty(attachmentUrl)){
                URL url = new URL(attachmentUrl);
                String filename = url.getFile();
                byte fileContent[] = null;
                //https://www.programcreek.com/java-api-examples/?api=org.springframework.mail.javamail.MimeMessagePreparator
                messageHelper.addAttachment(filename,new ByteArrayResource(fileContent));
            }
        };
        emailSender.send(messagePreparator);
    }
}
