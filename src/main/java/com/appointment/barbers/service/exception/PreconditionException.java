package com.appointment.barbers.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Use when the user has to meet a precondition before executing this action
 * Http status 412
 */
@ResponseStatus(HttpStatus.PRECONDITION_FAILED)
public class PreconditionException extends RuntimeException {

    public PreconditionException() {
    }

    public PreconditionException(String message) {
        super(message);
    }

    public PreconditionException(String message, Throwable cause) {
        super(message, cause);
    }

    public PreconditionException(Throwable cause) {
        super(cause);
    }

    public PreconditionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}