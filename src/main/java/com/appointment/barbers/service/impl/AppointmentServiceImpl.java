package com.appointment.barbers.service.impl;

import com.appointment.barbers.model.Appointment;
import com.appointment.barbers.repository.AppointmentRepository;
import com.appointment.barbers.service.AppointmentService;
import com.appointment.barbers.service.email.EmailService;
import com.appointment.barbers.service.exception.NotFoundException;
import org.springframework.transaction.annotation.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Transactional
public class AppointmentServiceImpl implements AppointmentService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private AppointmentRepository appointmentRepository;
    private EmailService emailService;

    public AppointmentServiceImpl(AppointmentRepository appointmentRepository, EmailService emailService) {
        this.appointmentRepository = appointmentRepository;
        this.emailService = emailService;
    }

    @Override
    public Appointment save(Appointment appointment) {
        Appointment savedAppointment = null;
        if (appointment == null ) {
            throw new NullPointerException("the appointment object may not be null");
        }
        if (appointment.getBarberId() != null) {
            logger.info("appointment going to save "+appointment.toString());
            savedAppointment = appointmentRepository.save(appointment);
            String bericht = "Uw reservatie is geboekt op "+appointment.getDate()+", de afspraak is nog niet bevestigd, wacht nog even tot as je de bevestiging krijgt";
            emailService.send(appointment.getEmail(),"uw reservatie is geboekt voor "+appointment.getDate(), bericht);
        }
        return savedAppointment;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Appointment> findAllByBarberIdOrderByDateDesc(Long barberId) {
        if (barberId != null && barberId > 0) {
            return this.appointmentRepository.findAllByBarberIdOrderByDateDesc(barberId);
        }
        throw new NullPointerException("The barber id may not null");
    }

    @Override
    @Transactional(readOnly = true)
    public Appointment findAppointmentById(Long appointmentId) {
        if (appointmentId != null) {
            return this.appointmentRepository.findByAppointmentId(appointmentId);
        }
        throw new NullPointerException("The appointmentId may not be null");
    }

    @Override
    @Transactional(readOnly = true)
    public Appointment findAppointmentByBarberIdAndAppointmentId(Long barberId, Long appointmentId) {
        if (barberId != null && appointmentId != null ) {
            return this.appointmentRepository.findAppointmentByBarberIdAndAppointmentId(barberId,appointmentId);
        }
        throw new NullPointerException("The appointment may not be null");
    }

    @Override
    public Appointment update(Appointment appointment) {
        if (appointment == null) {
            throw new NullPointerException("Appointment may not be null");
        }
        Appointment savedAppointment = this.findAppointmentById(appointment.getAppointmentId());
        if (savedAppointment != null ){
            logger.info("exisiting appointment found :"+savedAppointment.getAppointmentId());
            this.save(appointment);
        }
        throw new NotFoundException("Appointment with "+appointment.getAppointmentId() + " id is not found");
    }

    @Override
    public void deleteByAppointmentId(Long appointmentId) {
        if (appointmentId != null && appointmentId > 0){
            Appointment app = this.findAppointmentById(appointmentId);
            if (app != null ){
                logger.info("exisiting appointment found :"+app.getAppointmentId());
                this.deleteByAppointmentId(appointmentId);
            }else {
                throw new NotFoundException("The appointment is not found: "+appointmentId);
            }
        }
        throw new NullPointerException("The appointmentId may not be null");
    }
}