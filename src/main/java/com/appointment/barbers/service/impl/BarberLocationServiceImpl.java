package com.appointment.barbers.service.impl;

import com.appointment.barbers.model.Barber;
import com.appointment.barbers.model.BarberLocation;
import com.appointment.barbers.repository.BarberLocationRepository;
import com.appointment.barbers.service.BarberLocationService;
import com.appointment.barbers.service.BarberService;
import com.appointment.barbers.service.exception.ForbiddenException;
import com.appointment.barbers.service.exception.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class BarberLocationServiceImpl implements BarberLocationService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private BarberLocationRepository barberLocationRepository;
    private BarberService barberService;

    public BarberLocationServiceImpl(BarberLocationRepository barberLocationRepository, BarberService barberService) {
        this.barberLocationRepository = barberLocationRepository;
        this.barberService = barberService;
    }

    @Override
    public BarberLocation save(BarberLocation barberLocation) {
        Barber barber = barberService.findBarberById(barberLocation.getBarberId());
        if (barber == null) {
            throw new NotFoundException("The barber is not found of the barberId: "+barberLocation.getBarberId());
        }

        BarberLocation savedBarberLocation = null;
        if (barberLocation != null) {
            savedBarberLocation = this.barberLocationRepository.save(barberLocation);
            return savedBarberLocation;
        } else {
            throw new ForbiddenException("You may not allowed to saved null object ");
        }
    }

    @Override
    @Transactional(readOnly = true)
    public BarberLocation findBarberLocationById(Long barberLocationId) {
        return this.barberLocationRepository.findByBarberLocationId(barberLocationId);
    }

    @Override
    public BarberLocation updateBarberLocation(BarberLocation barberLocation) {
        if (barberLocation.getBarberId() == null) {
            throw new NotFoundException();
        }
        BarberLocation oldBarberLocation = this.barberLocationRepository.findByBarberId(barberLocation.getBarberId());
        logger.info("Barber location "+oldBarberLocation.toString() + " updated barberLocation: "+ barberLocation.toString());
        return barberLocationRepository.save(barberLocation);
    }

    @Override
    public void deleteBarberLocation(Long barberLocationId) {
        BarberLocation barberLocation = this.barberLocationRepository.findByBarberLocationId(barberLocationId);
        if (barberLocation != null) {
            this.barberLocationRepository.deleteByBarberLocationId(barberLocation.getBarberLocationId());
        } else {
            throw new NotFoundException();
        }
    }

    @Override
    public BarberLocation findByBarberId(Long barberId) {
        if (barberId != null && barberId > 0) {
            BarberLocation barberLocation = this.barberLocationRepository.findByBarberId(barberId);
            if (barberLocation != null ){
                return barberLocation;
            } else {
                throw new NotFoundException();
            }
        }else {
            throw new ForbiddenException("The barberid id may not null or less than or equal to 0");
        }
    }

    @Override
    public List<BarberLocation> findByPostcode(String postcode) {
        if (postcode != null && !postcode.trim().isEmpty()) {
            List<BarberLocation> barberLocations = this.barberLocationRepository.findByPostCode(postcode);
            if (barberLocations != null) {
                return barberLocations;
            }else {
                throw new NotFoundException();
            }
        }else {
            throw new ForbiddenException("The postcode  may not null ");
        }
    }
}