package com.appointment.barbers.service.impl;

import com.appointment.barbers.model.OpeningsHour;
import com.appointment.barbers.repository.BarberOpeningsHourRepository;
import com.appointment.barbers.service.BarberOpeningsHourService;
import com.appointment.barbers.service.exception.ForbiddenException;
import com.appointment.barbers.service.exception.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class BarberOpeningsHourServiceImpl implements BarberOpeningsHourService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private BarberOpeningsHourRepository barberOpeningsHourRepository;

    public BarberOpeningsHourServiceImpl(BarberOpeningsHourRepository barberOpeningsHourRepository) {
        this.barberOpeningsHourRepository = barberOpeningsHourRepository;
    }

    @Override
    public OpeningsHour save(OpeningsHour openingsHour) {
        OpeningsHour savedOpeningsHour = null;
        if (openingsHour != null){
            savedOpeningsHour = this.barberOpeningsHourRepository.save(openingsHour);
            return  savedOpeningsHour;
        }else {
            throw new ForbiddenException("You may not allowed to saved null object");
        }
    }

    @Override
    @Transactional(readOnly = true)
    public OpeningsHour findByOpeningsHourId(Long openingsHourId) {
        if (openingsHourId != null && openingsHourId >0){
            OpeningsHour openingsHour = this.barberOpeningsHourRepository.findByOpeningsHourId(openingsHourId);
            if (openingsHour != null){
                return openingsHour;
            }else {
                throw new NotFoundException();
            }
        }else {
            throw new ForbiddenException("OpeningshoursId may not null or less than 0");
        }
    }

    @Override
    @Transactional(readOnly =  true)
    public List<OpeningsHour> findByBarberId(Long barberId) {
        if (barberId != null && barberId >0) {
            List<OpeningsHour> openingsHours = this.barberOpeningsHourRepository.findByBarberId(barberId);
            if (openingsHours != null) {
                return openingsHours;
            }else {
                throw new NotFoundException();
            }
        }
        throw new ForbiddenException("The barberId may not null or less than 0");
    }

    @Override
    public OpeningsHour update(OpeningsHour openingsHour) {
        if (openingsHour.getOpeningsHourId() != null  && openingsHour.getOpeningsHourId() > 0) {
            OpeningsHour orginalOpeningsHours = this.barberOpeningsHourRepository.findByOpeningsHourId(openingsHour.getOpeningsHourId());
            if (orginalOpeningsHours != null) {
                logger.info("Openingshours  " + orginalOpeningsHours.toString() + " updated openingshours: " + openingsHour.toString());
                OpeningsHour updatedOpeningshours = this.barberOpeningsHourRepository.save(openingsHour);
                return updatedOpeningshours;
            } else {
                throw new NotFoundException();
            }
        }
        throw new ForbiddenException("Openingshour may not null or openingshourId may not null");
    }

    @Override
    public List<OpeningsHour> saveOpeningsHours(List<OpeningsHour> openingsHours) {
        return this.barberOpeningsHourRepository.save(openingsHours);
    }

    @Override
    public List<OpeningsHour> update(List<OpeningsHour> openingsHours) {
        if (openingsHours != null && openingsHours.size()>0) {
        return this.barberOpeningsHourRepository.save(openingsHours);
        }
        throw new ForbiddenException("Openingshours object may not be null or empty");
    }

    @Override
    public void deleteOpeningshourById(Long openingsHourId) {
        OpeningsHour findOpeningshour = this.findByOpeningsHourId(openingsHourId);
        if (findOpeningshour != null) {
            this.deleteOpeningshourById(openingsHourId);
        }
    }
}