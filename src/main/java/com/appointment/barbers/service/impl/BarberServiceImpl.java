package com.appointment.barbers.service.impl;

import com.appointment.barbers.model.Barber;
import com.appointment.barbers.repository.BarberRepository;
import com.appointment.barbers.service.BarberService;
import com.appointment.barbers.service.exception.ForbiddenException;
import com.appointment.barbers.service.exception.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@Transactional
public class BarberServiceImpl implements BarberService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private BarberRepository barberRepository;

    public BarberServiceImpl(BarberRepository barberRepository) {
        this.barberRepository = barberRepository;
    }

    @Override
    public Barber save(Barber barber) {
        Barber savedBarber = null;
        if (barber != null ) {
            savedBarber  = this.barberRepository.save(barber);
            return  savedBarber;
        } else {
            throw new ForbiddenException("You may not allowed to saved null object ");
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<Barber> findAllMenBarber() {
        return this.barberRepository.findByIsMen(true);
    }

    @Override
    public List<Barber> findAllBarbers() {
        return this.barberRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Barber findBarberById(Long barberId) {
        return this.barberRepository.findByBarberId(barberId);
    }

    @Override
    @Transactional
    public Barber updateBarber(Barber barber) {
        if (barber.getBarberId() == null) {
            throw new NotFoundException();
        }
        Barber oldBarber = this.findBarberById(barber.getBarberId());
        logger.info("Barber "+ oldBarber.toString() + " updated barber: "+barber.toString());
        return barberRepository.save(barber);
    }

    @Override
    @Transactional
    public void deleteBarber(Long barberId) {
        Barber barber = this.findBarberById(barberId);
        if (barber == null) throw new NotFoundException();
        /**
         * Before deleting the barber object make sure you remove all the services of the barber
         * send notification of cancelled to all appointment of that specific barber and delete all the appointments of the barber
         * delete the barber location
         * delete the barber openingshour
         * */
        this.barberRepository.delete(barber);
    }
}