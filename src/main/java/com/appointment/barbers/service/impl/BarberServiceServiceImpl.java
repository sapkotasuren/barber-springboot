package com.appointment.barbers.service.impl;

import com.appointment.barbers.model.BarberService;
import com.appointment.barbers.repository.BarberServiceRepository;
import com.appointment.barbers.service.BarberServiceService;
import com.appointment.barbers.service.exception.ForbiddenException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@Transactional
public class BarberServiceServiceImpl implements BarberServiceService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private BarberServiceRepository barberServiceRepository;

    public BarberServiceServiceImpl(BarberServiceRepository barberServiceRepository){
        this.barberServiceRepository = barberServiceRepository;
    }

    @Override
    public BarberService save(BarberService barberService) {
        BarberService savedBarberService = null;
        if (barberService != null ){
            savedBarberService = this.barberServiceRepository.save(barberService);
            return savedBarberService;
        } else {
            throw new ForbiddenException("You may not allowed to saved null object ");
        }
    }

    @Override
    @Transactional(readOnly = true)
    public BarberService findBarberServiceByBarberIdAndBarberServiceId(Long barberId, Long barberServiceId) {
        if (barberId == null || barberServiceId == null) {
            throw new ForbiddenException("The barberId and barservicedId may not be null");
        }
        return this.barberServiceRepository.findBarberServiceByBarberIdAndBarberServiceId(barberId,barberServiceId);
    }

    @Override
    public BarberService findBarberServiceByBarberServiceId(Long barberServiceId) {
        if (barberServiceId == null) {
            throw new ForbiddenException("The BarberServiceId may not be null or empty");
        }
        return this.barberServiceRepository.findBarberServiceByBarberServiceId(barberServiceId);
    }

    @Override
    public List<BarberService> findBarberServiceByBarberId(Long barberId) {
        if (barberId == null) {
            throw new ForbiddenException("The barberId may not be null");
        }
        return this.barberServiceRepository.findBarberServiceByBarberId(barberId);
    }

    @Override
    public void deleteBarberServiceByBarberId(Long barberId) {
        if (barberId == null) {
            throw new ForbiddenException("The barberId may not be null");
        }
        this.barberServiceRepository.deleteBarberServiceByBarberId(barberId);
    }

    @Override
    public void deleteBarberServiceByBarberServiceId(Long barberServiceId) {
        if (barberServiceId  == null) {
            throw new ForbiddenException("The barberServiceId may not be null");
        }
        this.barberServiceRepository.deleteBarberServiceByBarberServiceId(barberServiceId);
    }

    @Override
    public BarberService updateBarberServiceByBarberServiceId(BarberService barberService) {
        if (barberService.getBarberId() == null || barberService.getBarberServiceId() == null) {
            throw new ForbiddenException("You may not allowed to update an object which doesn't exist in db!");
        }
        BarberService savedBarberService =  this.barberServiceRepository.save(barberService);
        return savedBarberService;
    }
}
