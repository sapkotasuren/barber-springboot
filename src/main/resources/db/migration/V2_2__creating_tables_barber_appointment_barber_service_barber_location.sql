CREATE TABLE barber(
	barber_id serial primary key,
	name varchar(255) not null,
	description varchar(255) ,
	isMen boolean not null,
	isWomen boolean not null
);

CREATE TABLE barber_location (
	barber_location_id serial primary key,
	street varchar(255) not null,
	house_number varchar(255) not null,
	bus varchar(255),
	postcode varchar(20) not null,
	city varchar(255) not null,
	country varchar(255) not null,
	barber_id_fk integer not null,
	foreign key (barber_id_fk) references barber (barber_id) on delete restrict
);

CREATE TABLE openings_hour (
	openings_hour_id serial primary key,
	day varchar(255) not null,
	m_from time,
	m_till time,
	a_from time,
	a_till time,
	isClose boolean,
	barber_id_fk integer not null,
	foreign key (barber_id_fk) references barber (barber_id) on delete restrict
);

CREATE TABLE barber_service (
	barber_service_id serial primary key,
	name varchar(255) not null,
	description varchar(255),
	price NUMERIC (5, 2) not null,
	gender char(1) not null,
	barber_id_fk integer not null,
	foreign key (barber_id_fk) references barber (barber_id) on delete restrict
);


CREATE TABLE appointment (
	appointment_id serial primary key,
	name varchar(255) not null,
	email varchar(255),
	mobile_phone varchar(150),
	gender char(1) not null,
	date_time timestamp not null,
	barber_id_fk integer not null,
	barber_service_id_fk integer not null,
	foreign key (barber_id_fk) references barber (barber_id) on delete restrict,
	foreign key (barber_service_id_fk) references barber_service (barber_service_id) on delete restrict
);