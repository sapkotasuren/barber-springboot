package com.appointment.barbers;

import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

import com.appointment.barbers.model.Barber;
import com.appointment.barbers.repository.BarberRepository;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest//(showSql = false)
@AutoConfigureTestDatabase(replace = NONE)
public class BarberRepositoryTest {

    @Autowired
    private BarberRepository barberRepository;
    private Barber barber = null;
    @BeforeEach
    @DisplayName("Setup method which run before each test method")
    public void setup() {
        barber = new Barber();
        barber.setIsMen(true);
        barber.setIsWomen(false);
        barber.setName("Men Barber");
        barber.setDescription("Men salon");
        Barber savedBarber = barberRepository.save(barber);
    }

    @Test
    @DisplayName("Trying to save barber object using barber repository")
    public void testSaveBarber() {
        Barber barber = new Barber();
        barber.setIsMen(true);
        barber.setIsWomen(true);
        barber.setName("Royal Barber");
        barber.setDescription("Unisex salon");
        Barber savedBarber = barberRepository.save(barber);
        assertEquals(savedBarber.getDescription(), barber.getDescription());
    }

    @Test
    @DisplayName("Trying to find barberbyId using barber repository")
    public void testFindBarberById() {
        Barber barber = barberRepository.findByBarberId(this.barber.getBarberId());
        assertEquals(barber, this.barber);
        assertTrue(barber.getIsMen());
    }

    @Test
    @DisplayName("Find all the barber of men")
    public void testFindByIsMen() {
        List<Barber> barberList = barberRepository.findAll();
        assertTrue(barberList.size() == 1);
        assertEquals(barberList.get(0), this.barber);
    }

    @Test
    @DisplayName("Find all barber of the system")
    public void testFindAllBarber() {
        List<Barber> barberList = barberRepository.findAll();
        assertTrue(barberList.size() == 0);
        assertEquals(barberList.get(0), this.barber);
    }





}
